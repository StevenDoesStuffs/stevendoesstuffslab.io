const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');

console.log(path.resolve('static'))

module.exports = {
    entry: path.resolve('src', 'js', 'index.ts'),
    target: 'web',
    output: {
        path: path.resolve('static', 'dist'),
        filename: 'index.js',
        library: 'app',
    },
    optimization: {
        minimizer: process.env.NODE_ENV === 'production' ? [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})] : [],
    },
    devtool: process.env.NODE_ENV === 'production' ? 'source-map' : 'eval-source-map',
    plugins: [new MiniCssExtractPlugin()],
    module: {
        rules: [{
            test: /\.css$/,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        publicPath: (resourcePath, context) => {
                            return path.relative(path.dirname(resourcePath), context) + '/';
                        },
                        hmr: process.env.NODE_ENV === 'development',
                    },
                },
                {
                    loader: 'css-loader', 
                    options: { 
                        importLoaders: 1 
                    } 
                },
                'postcss-loader',
            ]
        }, {
            test: /\.m?ts$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-typescript']
                }
            }
        }]
    }
};
