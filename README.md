# stevendoesstuffs.gitlab.io

[Link](https://blog.stevendoesstuffs.dev)

Dependencies: yarn, hugo

## Building

```bash
yarn # installs all the packages, only needs to be run once
yarn build # bundles resources into /static/dist/
hugo # builds the website
```

## Development

```bash
yarn # installs all the packages, only needs to be run once
yarn watch # watches and rebuilds resources
hugo serve -D # serves and rebuilds the website
```
