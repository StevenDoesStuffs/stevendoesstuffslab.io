import '../css/main.css'

let imgFont = 80;
let maxScale = 1.75;
let maxRes = 48;
let minScale = 1.125;
let minRes = 30;

export function toggleClass(group_class: string, toggle: string) {
    for (let elem of document.getElementsByClassName(group_class)) {
        if (elem.classList.contains(toggle)) {
            elem.classList.remove(toggle);
        } else {
            elem.classList.add(toggle);
        }
    }
}

function scaleImg() {
    let rootSize = parseFloat(window.getComputedStyle(document.documentElement).fontSize);
    let windowWidth = document.documentElement.clientWidth / rootSize; // width in REM
    let linear = (windowWidth - minRes)*((maxScale - minScale) / (maxRes - minRes)) + minScale;
    let targetRem = Math.min(maxScale, Math.max(minScale, linear)); // target font size in img in rem
    let currentRem = imgFont / rootSize; // current font size in img in rem
    return targetRem / currentRem;
}

function wrapImg(elem: Element) {
    let parent = elem.parentElement;
    let wrapper = document.createElement('span');

    wrapper.classList.add('post-img-wrapper');
    parent.insertBefore(wrapper, elem);
    wrapper.appendChild(elem);
}

window.addEventListener('load', (_) => {
    let scale = scaleImg();
    for (let elem of document.querySelectorAll('.post img')) {
        wrapImg(elem);
        let img = elem as HTMLImageElement;
        img.srcset = img.src + ' ' + (1/scale) + 'x';
        img.src = '';
    }
});

window.addEventListener('resize', (_) => {
    let scale = scaleImg();
    for (let elem of document.querySelectorAll('.post-img-wrapper img')) {
        let img = elem as HTMLImageElement;
        img.srcset = img.srcset.split(' ')[0] + ' ' + (1/scale) + 'x';
    }
});

for (let elem of document.querySelectorAll('code')) {
    let content = elem.textContent.trim();
    let parent = elem.parentElement;
    if (content.startsWith('$') && content.endsWith('$') && elem.childNodes.length === 1 &&
        elem.children.length === 0 && parent.tagName !== "PRE") {
        
        parent.insertBefore(elem.firstChild, elem);
        elem.remove();
    }
}

(window as any).MathJax = {
    tex: {
        inlineMath: [['$', '$'], ['\\(', '\\)']],
        displayMath: [['$$','$$'], ['\\[', '\\]']],
        processEscapes: true,
        processEnvironments: true
    }
};

let mathjax = document.createElement('script');
mathjax.src = 'https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js';
mathjax.async = true;
document.head.appendChild(mathjax);
