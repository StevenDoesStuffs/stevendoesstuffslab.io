---
title: "Intro to Open Addressing and Robin Hood Hashing"
date: 2020-07-25
draft: false
markup: md
tags: ["computer-science", "data-structures"]
author: "Steven Xu"
---

Generally, I want people to be able to read all my blog posts without having any background knowledge of the topic, 
so I'm writing a this article as a part 1 to a more advanced article on robin hood hashing which you can read 
[here](/posts/fast-resizing-robin-hood-hashing). 
If you already know how robin hood hashing works, feel free to skim or skip this post. 

<!--more-->

# Hashtables and Open Addressing

A hashtable is a data structure for creating associative arrays, 
which are arrays indexed by arbitrary data rather than traditional numerical indices. 
For example, I might want an array storing birthdays (`String`) indexed by people's names (`String`). 
In the context of hashtables, we call the index the key, its associated value the value, 
and the key-value pair the entry (which I'll generally write as `key = value`). 

Hashtables require a hash function to operate, which is a function mapping keys to integers. 
Good hash functions are random uniform distributions, meaning they map each key to a completely random integer.

Hashtables are backed by a regular array (which we'll call `data`) of "buckets" which each can store an entry.
There is another type of hashtable which doesn't do that called "separate chaining" hashtables, but we're going to ignore that for the this article. 
The "load factor" is the proportion of buckets that are occupied. 
Each key is associated with a bucket using the function `bucket(key) = data[hash(key) % n]`, where `n` is the length of the hashtable. 
In order to `insert` an entry into a hashtable, we insert the key and the value into `bucket(key)`, 
when we want to `get` a value given a key, we return the value stored in `bucket(key)`, 
and to `remove` an entry from the hashtable, we clear `bucket(key)`. 

Our hashtables must be backed by fixed length arrays since computing the bucket of a key requires the length of the array.
This means that we'll need to resize our hashtable as we insert more items. 

Often two keys end up with the same index or "collide". 
The way we handle a hash collision is what's called collision resolution. 
Different collision resolution systems have various tradeoffs, and different languages implement hashtables differently.
Also, for the purposes of collision resolution, we generally think of the array as a circle, so the bucket "after" `data[n - 1]` is `data[0]`. 

# Linear Probing

One such collision resolution system is called linear probing. 
When inserting, if `bucket(key)` is occupied, we try putting our entry into the _next_ empty bucket. When running `get`, 
we start at `bucket(key)` and successively try the next bucket until we find our entry (and return the value) or reach an empty bucket (and return null). 
Delete is a bit complicated for linear probing, and since the focus of this article is robin hood hashing, I'll leave it out. 

The "home bucket" is `bucket(key)`, the distance between the $a$th and $b$th buckets is defined as $b - a \pmod{n}$ (not commutative),
and it's the number of buckets we'd have to visit if we walked forward starting from bucket $a$ until we got to bucket $b$. 
The "probing distance" of an entry is the distance between the home bucket and the bucket it's currently in. 

As usual, here's an example. 
Let's see what's going on inside a hashtable where the keys are names (type `String`) and the values are birthdays (also type `String`). 
We'll use a length of 6 for our backing array, and we'll use the hash function below:

| `key`      | `hash(key)` |
|------------|-------------|
| `"Jane"`   | `0`         |
| `"Olivia"` | `1`         |
| `"Brian"`  | `5`         |
| `"Alex"`   | `6`         |
| `"Jake"`   | `11`        |

Note that hash functions are typically supposed be able to take as input _all_ possible values of a type (in our case `String`), 
and that they're supposed to output random values in the full range of the integer type. 
Our example does neither of those things, but it's convenient to work with when illustrating. 

Anyhow, then we'll run the following code:

```rust
let mut hashtable; // this is our hashtable that we'll be inserting into

hashtable.insert("Olivia", "3/24");
hashtable.insert("Jake", "6/19");
hashtable.insert("Jane", "10/8");
hashtable.insert("Alex", "12/1");
hashtable.insert("Brian", "8/10");

hashtable.get("Alex");
```

---

We start off with an empty hashtable. Below is a depiction of the array of buckets.
![Empty Hashtable](linear-probing/empty.svg)

---

Then we insert `"Olivia" = "3/24"`. The hash of the key is $1$, and since $1 \pmod{n} = 1 \pmod{6} = 1$, we insert it into bucket 1. 
Notice that the each cell (bucket) shows the hash of its entry's key (if not empty). 
In practice, we don't actually store the hash of the key. 
![Insert "Olivia"](linear-probing/insert-1.svg)

---

Next we insert `"Jake" = "6/19"`. The hash of the key is $11$, and since $11 \pmod{6} = 5$, we insert it into bucket 5.
![Insert "Jake"](linear-probing/insert-2.svg)

---

Next we insert `"Jane" = "10/8"`. The hash of the key is $0$, and since $0 \pmod{6} = 0$, we insert it into bucket 0.
![Insert "Jane"](linear-probing/insert-3.svg)

---

So far, we haven't had any collisions, but that won't always be the case, so let's see what happens when we get one. 
Our next step is to insert `"Alex" = "12/1"`. The hash of the key is $6$, and since $6 \pmod{6} = 0$, we insert it into bucket 0.
But bucket 0 (our home bucket) is occupied! So we keep on moving forward until we find the next empty bucket, and we insert it there.
In this case, the next empty bucket is bucket 2. 
![Insert "Alex"](linear-probing/insert-4.svg)

---

Finally, we insert `"Brian" = "8/10"`. The hash of the key is `5`, and since $5 \pmod{6} = 5$, we insert it into bucket `5`. 
Unfortunately, we also run into a collision there. It's also the end of the array, so we don't really have a "next" bucket, 
but we can resolve this by thinking of the array as a circle and say that the next bucket is 0 and continue from there. 
That bucket's occupied as well, so we keep going until we reach bucket 3, the next empty bucket, and insert it there. 
![Insert "Brian"](linear-probing/insert-5.svg)

---

After we're done inserting, we try to get the the value of the entry with the key `"Alex"`. 
We find that the home bucket of that key is bucket 0, since the hash of the key is 6, and $6 \pmod{6} = 0$.
But we look at bucket 0, and we find that the key there isn't equal to `"Alex"`, so we move onto the next bucket.
We keep moving forward until we hit an empty bucket, at which point we know that there is no entry with the key `"Alex"` in our hashtable,
or we find a bucket with a key of `"Alex"`. In our case, we find that bucket 2 has that key, 
so we return the value of bucket 2, which is `"12/1"`. The whole process visited buckets 0, 1, and 2. 
![Insert "Brian"](linear-probing/get.svg)

# Robin Hood Hashing

One problem with linear probing is that sometimes our probing distance gets too big. 
In the previous example, the entry with the key `"Brian"` had a probing distance of 4! 
This means `hashtable.get("Brian")` would've had to visit five buckets (four incorrect buckets and one correct one) before it found the correct one. 
Robin hood aims to decrease the maximum probing distance by making the insert function more "fair". 

Robin hood also requires us to store the probing distance of each bucket, so I'll expand the images to include that.
For example, here's what our hashtable looked like at the end of the previous example but with the probing distances shown. 
![Previous Example With Distances](robin-hood/linear-dist.svg)

Notice how the probing distance of any entry is the index of the bucket it's in minus the index of its home bucket, mod $n$.

Anyhow, here's how insert works for robin hood. As usual, we start at the home bucket, but as we visit each bucket, 
we compare of the probing distance of the entry in the bucket we're at (the occupying distance) 
to the probing distance of the entry we're inserting if we were to put it in that bucket (the inserting distance). 
If the former is less than the latter, we swap the occupying entry and the inserting entry and continue the insert. 

Let's see an illustration of how it works. We're going to do the fourth insert from the previous example. 
For reference, we're running this line: `hashtable.insert("Alex", "12/1")`, and the hashtable currently looks like this:
![Hashtable Before Fourth Insert](robin-hood/before-insert.svg)

---

We need to insert `"Alex" = "12/1"`, and its hash is 6 which means its home bucket is 0, so we try to insert it there first.
![First Step of Insert](robin-hood/insert-4-1.svg)

---

That bucket is occupied, so our next step is to compare the probing distances. The occupying distance is equal to the inserting distance, 
so we move on to the next bucket.
![Second Step of Insert](robin-hood/insert-4-2.svg)

---

The next bucket is occupied as well, so again we compare the probing distances. 
This time, the occupying distance (0) is less than the inserting distance (1), so we swap the inserting entry with the occupying entry. 
![Third Step of Insert](robin-hood/insert-4-3.svg)

---

Then we move onto the next bucket, which happens to be empty. We can then put our inserting entry there. 
![Fourth Step of Insert](robin-hood/insert-4-4.svg)

---

When using linear probing, the maximum probing distance after the fourth insert was 2, but with robin hood, it's now 1.

# Part Two

If you're interested in learning more, there is a part 2 to this post which you can read [here](/posts/fast-resizing-robin-hood-hashing).
