---
title: "Introduction to Tree Lists"
date: 2020-10-31
draft: false
markup: md
tags: []
author: "Steven Xu"
---

Tree lists are a really cool data structure which allows list operations that normally take $O(n)$ time, like remove or insert, to be done in $O(\log n)$ time. 
It's kind of a niche data structure, but despite its relative obscurity, tree lists have some really cool applications.
For example, you could implement a dynamically resizing segment trees or do bulk operations, both of which I'll cover in a future post. 

This post will also assume you solidly understand regular binary search trees and know a little bit about self balancing binary search trees.
Just like in the most of my other articles, **I'll be using inclusive starts and exclusive ends for intervals**. 
All the code will be python.

<!--more-->

# Tree Lists

A tree list is a list built over a binary search tree, and it will be able to do all normal list operations. 
Specifically, we'll want these methods: `get(index)`, `update(index, val)`, `insert(index, val)`, `remove(index)`. 
What makes the tree list so special is that normally, inserting into or deleting an element in the middle of a list is an $O(n)$ operation, 
but for a tree list, it's $O(\log n)$. 

Each element in the list is represented by exactly one node in the BST, and the BST is ordered by the _index_ of the elements. 
That means the order of elements in the list is the same as the order in which an in-order traversal visits the nodes. 
For example, the tree below represents the list `[1, 6, 4, 7, 0, 9, 2, 5, 3, 8]`. 
![Tree List Example](tree-list/example-traversal.svg)
The in-order traversal visits nodes in the order the the nodes' dots meet the blue line
([source](https://www.wikiwand.com/en/Tree_traversal#/Depth-first_search_of_binary_tree)).

To implement our tree list, each node will have to keep track of how many descendants it has + itself. 
For example, the node with index 6 and value 9 in the tree list above as a count of 3, since it has 2 descendants + itself = 3. 
We'll call this value `node.count`. On top of that, let's add a few methods we'll need later.
Also, to keep things simple, our tree list will only support traditional indexing. 

Currently, our node class looks like this:
```python
class TreeList:
    def __init__(self, val):
        self.val = val
        self.count = 1
        self.dir = None
        self.parent = None
        self.children = [None, None]
```
The `parent`, `children`, and `val` variables are standard to any binary search tree, and `count` is as described previously.
The `dir` variable stores the direction of the current node relative to the parent. 
If the current node is the left child of its parent, `dir` is equal to zero, and otherwise, it's one. 

Whenever we modify the tree's structure, we'll need up update the `count` variable for the affected node. 
For example, if we attach a single node to a leaf, that leaf's `count` used to be one, but now it needs to be updated to `two`. 
Then because that node's count was changed, we need to update the count of that node's parent, grandparent, ..., all the way up to the root.
We'll call this method `propagate_count`.
```python
def count(node):
    return node.count if node is not None else 0

def propagate_count(self):
    old = self.count
    self.count = count(self.children[0]) + 1 + count(self.children[1])
    if self.parent is not None and self.count != old:
        self.parent.propagate_count()
```
<p></p>

From there, we'll modify the structure of the tree only through one method, which we'll call `set_child`. 
The method will set the child in the direction given to the node given and return the old child in that direction.
It'll also call `propagate_count` for us.
```python
def set_child(self, child, dir):
    if self.children[dir] is not None:
        self.children[dir].parent = None
        self.children[dir].dir = None
    self.children[dir] = child
    if child is not None:
        child.parent = self
        child.dir = dir
    self.propagate_count()
```
<p></p>

There are a few trivial getters and setters left, namely `set_val`, `set_left`, `set_right`, `get_right`, and `get_left`, 
but I'll omit the code for those to keep this post short. `set_left` and `set_right` simply call `set_child`, 
`get_left` and `get_right` are there for convenience, and `set_val` will be neccesary in the future (next blog post). 

One thing to note is that our tree list can't really by empty if we use `Node` directly, 
since any node that exists must have a value and a count greater than or equal to 1, 
so in order to expose a more usable interface, it's a good idea to wrap our node class in another class.

## Get

Say we want to find the node with index `i` in a tree rooted at `node`. 
Using `count`, we can find the number of nodes in the left subtree (which we'll call `left`). 
- If `i < left`, then the node with index `i` must be in the left subtree, and we recurse on the left child. 
- If `i == left`, then the `i`th node must be the current node. 
- If `i > left`, then we pretend we've traversed `left + 1` nodes 
  (`left` for the left subtree and `1` for the current node), 
  subtract that off from the index, and recurse on the right child. 

<p></p>

This leads us to the following algorithm for `get(index)`:
```python
def get(self, index):
    if index < 0 or index >= self.count: 
        raise IndexError
    
    left = count(self.get_left())
    if index < left: 
        return self.get_left().get(index)
    elif index == left: 
        return self
    else: 
        return self.get_right().get(index - (left + 1))
```
<p></p>

Since we traverse the entire left subtree first in an in-order traversal, if the left subtree has `>= i` number of nodes, 
then the `i`th node must be somewhere in the left subtree, and we go look there. 
Otherwise, it's either the current node or in the right subtree. 
Either way, we can tell which subtree it's in, so we never have to go down both.

The get method returns a node instead of a value for use in other methods, 
but getting the value from the node is trivial. 

Let's see the algorithm in action. We'll run `get(5)` on the tree from the previous example. 
The picture below traces the recursive calls of `get`, 
displaying `index` in orange next to `node` whenever `node.get(index)` is called. 
![Tree List Get Example](tree-list/example-get.svg)
The algorithm stops whenever it reaches a node whose left child has a count equal to the index, 
which in our case is indeed at index 5 (value 9) since we're looking for an index of 1 by the time we get there,
and its left child has a count of 1.

## Update

With `get(index)` implemented, we can now figure out where in the tree an index is. 
This allows us to easily implement the other three algorithms.

Here's the algorithm for `update(index, val)`:
```python
def update(self, index, val):
    self.get(index).set_val(val)
```
Since it's fairly simple, I'll omit the explanation and examples. 

## Only Child Theorem

Before we continue, we'll need to prove the following about in-order traversal—if a node has a (left | right) child, 
then the in-order (predecessor | successor) is the (right | left)-most descendant of that child and therefore has no (right | left) child.
The (left | right)-most descendant of the node is the node reached by going (left | right) until there's no child in that direction. 

Here's a visual proof of the theorem (the dotted line means there can be zero or more nodes in between going that direction): 
<span class="post-img-flex">
![Predecessor Proof](tree-list/predecessor-proof.svg)![Successor Proof](tree-list/successor-proof.svg)</span>
The above image shows that in an in-order traversal, there is nothing between a node and the 
(right | left) most descendant of its (left | right) child. 
From here onwards, we'll refer to this fact as the only child theorem (made up name). 

## Insert

Finally, we're ready for the algorithm for `insert(index, val)`. Remember that when we're inserting a value at a given index, 
we're shifting everything over by one. This means that whatever we're trying to insert will come before the value currently at that index,
but after the value currently before that index. 

Anyhow, first we have to figure out which node in the tree `index` is. 
We can do that using `get`, and let's call it `current`. But before then, we have to check that our index is actually in bounds. 
If our `index` is equal to the length of the list, we insert it at the end of the list. Otherwise, if it's out of bounds, we can just let `get` error.

From here, we know `current` isn't `None`. Our goal now is to insert `val` before `current`. 
If `current` has no left child, then we can just insert our value there, and it'll end up being the node right before `current`. 
Otherwise, we need to find the in-order predecessor, which is just the node at `index - 1`, and insert our value after that.
Since we know that node has no right child by the only child theorem (it's the predecessor of a node with a left child), we can insert our value there. 

Our code is then:
```python
def insert(self, index, val):
    if index == self.count:
        self.get(self.count - 1).set_right(Node(val))
        return
    
    current = self.get(index)
    if current.get_left() is None:
        current.set_left(Node(val))
    else:
        self.get(index - 1).set_right(Node(val))
```
<p></p>

Let's see it in action—we'll run `insert(1, 5)` on our example tree. The `current` node, highlighted in blue, has a left child.
This means we get the node at `index - 1 = 0`, highlighted in green, and insert our value as the right child of that node, highlighted in orange. 
![Tree List Insert Example](tree-list/example-insert.svg)

## Remove

Finally, we need to implement an algorithm for `remove(index)`. 
The algorithm we'll use is actually just the standard BST delete algorithm (see [here](https://www.wikiwand.com/en/Binary_search_tree#/Deletion)).
The idea is that if can't detach the node at `index` because it has more than one child, 
we swap it with either the in-order predecessor or successor and remove that instead.

First, we have to get the node we're trying to remove; we'll call this `current`. If `current` has both a left and a right child, 
we can swap it with either its predecessor or successor, both of which will only have one child by the only child theorem, and then remove that instead. 
Otherwise, if it only has one child, we can just replace it with its child. 
Finally, if it has no children, then we can simply detach it. 

A visual demonstration of the two non-trivial cases is shown below, where we're trying to remove a node highlighted in green:
![Tree List Remove Cases](tree-list/remove-cases.svg)

Finally, here's the code:
```python
def remove(self, index):
    current = self.get(index)
    # two children case
    if current.get_left() is not None and current.get_right() is not None:
        next = self.get(index + 1)
        current.set_val(next.val)
        current = next
    # replace current node with either nothing (zero children case)
    # or the only child of the current node (one child case)
    child = None
    if current.children != [None, None]:
        child = (get_right() 
            if current.get_left() is None 
            else current.get_left())
    current.parent.set_child(child, current.dir)
```

## Performance

Recall that at the start of this article, we said that the runtime of all our operations should be $O(\log n)$. 
Unfortunately, that's not exactly true. As with any other BST, the runtime of all our methods above is actually $O(h)$,
where $h$ is the height of the tree. In order to achieve $O(\log n)$, 
the tree list must be combined with a self balancing mechanism of some sort (treap, AVL, red-black, or other).
It's fairly easy to do but simultaneously beyond the scope of this post, so I'll just leave it at that, 
but it's definitely something to keep in mind during implementation. 

# Conclusion

The tree list is a list data structure built over a BST which allows you to do insert and remove in $O(\log n)$ instead of $O(n)$.
The tradeoff is that on top of having BST and self balancing overhead, operations that are normally $O(1)$, like get, are now also $O(\log n)$.
Most of the time, people choose to use a skip list over a tree list, as it provides equivalent asymptotic performance but is better in other ways.

Tree lists have some really cool applications, which I'll cover in my next blog post. As a sneak peak, I'll quickly tease them here.
- Tree lists can do bulk operations. It's possible to take a tree list and insert it into the middle of another tree list, all in $O(\log n)$ time. 
- Tree lists can be used to build dynamically resizing segment trees. Segment trees solve the range query problem for a static list, 
  but they don't work if you need to insert and remove elements in between queries. 
  In a dynamically resizing segment tree, it's possible to achieve all list operations and query in $O(\log n)$. 
- Despite having $O(\log n)$ access, tree lists can still do many things, such as sorting, in the same time as regular arrays. 
<p></p>

Finally, the code is available [here](TreeList.py). 
This code shouldn't be used anywhere where you need functioning code.
