def count(node):
    return node.count if node is not None else 0

class TreeList:
    def __init__(self, val):
        self.val = val
        self.count = 1
        self.dir = None
        self.parent = None
        self.children = [None, None]
    
    # this method updates `node.count` for itself,
    # then its parent, then its grandparent, etc.
    def propagate_count(self):
        old = self.count
        self.count = count(self.children[0]) + 1 + count(self.children[1])
        if self.parent is not None and self.count != old:
            self.parent.propagate_count()
    
    # sets the child in the direction dir, 
    # where dir == 0 means left and dir == 1 means right
    def set_child(self, child, dir):
        if self.children[dir] is not None:
            self.children[dir].parent = None
            self.children[dir].dir = None
        self.children[dir] = child
        if child is not None:
            child.parent = self
            child.dir = dir
        self.propagate_count()
    
    # this method seems useless, but we'll override it later
    def set_val(self, val):
        self.val = val

    # make our lives a bit easier
    def get_left(self):
        return self.children[0]
    def set_left(self, child):
        self.set_child(child, 0)
    def get_right(self):
        return self.children[1]
    def set_right(self, child):
        self.set_child(child, 1)
    
    def get(self, index):
        if index < 0 or index >= self.count: 
            raise IndexError
        
        left = count(self.get_left())
        if index < left: 
            return self.get_left().get(index)
        elif index == left: 
            return self
        else: 
            return self.get_right().get(index - (left + 1))

    def update(self, index, val):
        self.get(index).set_val(val)
    
    def insert(self, index, val):
        if index == self.count:
            self.get(self.count - 1).set_right(Node(val))
            return
        
        current = self.get(index)
        if current.get_left() is None:
            current.set_left(Node(val))
        else:
            self.get(index - 1).set_right(Node(val))

    def remove(self, index):
        current = self.get(index)
        # two children case: swap current node with successor or predecessor
        if current.get_left() is not None and current.get_right() is not None:
            next = self.get(index + 1)
            current.set_val(next.val)
            current = next
        # replace current node with either nothing (zero children case)
        # or the only child of the current node (one child case)
        child = None
        if current.children != [None, None]:
            child = (get_right() 
                if current.get_left() is None 
                else current.get_left())
        current.parent.set_child(child, current.dir)
