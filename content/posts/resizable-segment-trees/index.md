---
title: "Resizable Segment Trees"
date: 2020-10-31T17:42:25-05:00
draft: true
markup: md
tags: []
author: "Steven Xu"
---


# Intro

A resizable segment tree is a segment tree augmented with two operations:

- `insert(index, val)` inserts `val` into `index`. This is _not_ the same as `update`, 
  as that method replaces whatever is at `index`, whereas `insert` shifts everything over.
- `remove(index)` removes the element at `index`. 

This makes for a total of 5 methods: `get(index)`, `update(index, val)`, `insert(index, val)`, `remove(index)`, `query(start, end)`. 

# Resizable Segment Trees

One interesting property about the tree list is that each node, on top of representing a single element in the list,
also represents the interval consisting of itself and all its descendants. For example, 
the node at index `7`, which has a value of `5`, represents the interval `4..10`, as shown in the picture below. 
![Intervals Example](media/intervals.svg)
<p></p>

We can see that each node represents a continuous interval since an in order traversal would reach that node,
visit it and all of its descendants, and then exit the node and never come back to it. 

In order to impose a segment tree structure on top of our tree list,
we'll store the fold of the interval represented by each node inside that node (which we'll call `node.fold`). 
From there, all the segment tree operations should become fairly obvious. 

Luckily for us, the way we've written the tree list is actually very extensible. 
All of our regular list methods use `set_child` (`set_left` or `set_right`) and `set_val` to modify the tree, 
so making those methods also propagate changes in the fold should be most of the changes we need to make. 
We also need to define the accumulation operation as well. 
Recall that this could be any associative operation that acts on the values of the nodes. 

Anyhow, the way we calculate the fold of a given node's interval is that it's equal to $l * v * r$,
where $l$ is the fold of the left node, $v$ is the value of the current node, $r$ is the fold of the right node,
and $*$ is the accumulation operation. In our code, we'll have to account for the possibility $l$ or $r$ might not exist.

Here's the code that calculates the fold:
```python
def op(a, b):
    # this could be any associate operation, 
    # but we're going to use max as an example
    return max(a, b)

def propagate_fold(self):
    old = self.fold
    # calculate fold
    self.fold = self.val
    if self.get_left() is not None
        self.fold = self.op(self.get_left().fold, self.fold)
    if self.get_right() is not None
        self.fold = self.op(self.fold, self.get_right().fold)
    # propagate it if necessary
    if self.parent is not None and self.fold != old:
        self.parent.propagate_fold()
```
<p></p>

Now we're actually ready to override `set_child` and `set_val`.
For each of these methods, all we have to do is call `propagate_fold` at the end.
```python
def set_child(self, child, dir):
    super().set_child(child, dir)
    self.propagate_fold()

def set_val(self, val):
    super().set_val(val)
    self.propagate_fold()
```

## Query

The only method that our tree list doesn't already have that our segment tree needs is `query`. 

