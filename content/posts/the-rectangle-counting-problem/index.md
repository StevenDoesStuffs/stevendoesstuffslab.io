---
title: "The Rectangle Counting Problem"
date: 2020-06-25
draft: false
markup: md
tags: ["computer-science", "algorithms", "competitive-programming"]
author: "Steven Xu"
---

Some time ago, I was watching [this](https://www.youtube.com/watch?v=EuPSibuIKIg) YouTube video. 
At the time, I could solve the first problem presented in the video, but I couldn't solve the second problem efficiently. 
After a lot of thinking, I've finally got the solution to the second problem, which I'll present here. 

<!--more-->

# The Problem.

Given a list of integer points in a 2d grid, count the number of rectangles you can form using those points. 
The rectangles don't necessarily have to be axis aligned, and the area of the rectangles must be nonzero. 
The order of the points in a rectangle doesn't matter. 

# Example

Input: 
```rust
[
    (0, 3),
    (1, 0), (1, 2),
    (2, 1), (2, 3), (2, 5),
    (3, 2), (3, 4),
    (4, 1), (4, 4),
    (5, 0), (5, 1), (5, 3),
    (6, 3), (6, 4),
    (7, 5)
]
```

This is what the input looks like when plotted:
![Points plotted](grid/points.svg)

Here are all the rectangles:
![Rectangles plotted](grid/rect.svg)

As you can see, there are six triangles in the example, 
so our algorithm should return `6` when run on the input.

# Solution

Consider a rectangle and its two diagonals. 
Note that the two diagonals are of equal length, and each diagonal is defined by two points. 
The diagonals intersect in the middle, and the intersection splits each diagonal in half. 
The intersection is also the center of the circumscribed circle of the rectangle. 
It's depicted below on the left: <span class="post-img-flex">
![Circumscribed Circle](circle/rect1.svg)![Circumscribed Circle](circle/rect2.svg)</span>

Notice how with just a single diagonal, 
we can determine what the circle is since the diagonal is actually a diameter of the circle.
Now consider how many rectangles can we form if we add an addition diagonal to the previous picture (colored yellow). 

On top of the rectangle from before, we can form two additional rectangles, depicted above on the right, for a total of three rectangles.
This is because any (unordered) pair of diagonals from the same circle forms a rectangle, 
so with three diagonals, we have three unique pairs and therefore three rectangles. 
In general, if we have $n$ diagonals from the same circle, we can form $\frac{n(n - 1)}{2}$ rectangles.

The algorithm works by iterating through all possible diagonals (unordered pairs of points) and grouping them by their circle. 
Once we have that, we loop through each circle and calculate the number of rectangles we can form from the diagonals. 
The sum of the number of rectangles from each circle is then our answer since each rectangle belongs to exactly one circle. 

We have to take extra care when storing the circles since we can't guarantee that the center point and radius of each circle will always be integers.
Let's say our diagonal has points $(x_1, y_1)$ and $(x_2, y_2)$. The center is then $(\frac{x_1 + x_2}{2}, \frac{y_1 + y_2}{2})$.
We can store it as a pair of integers by not dividing by two, instead using $(x_1 + x_2, y_1 + y_2)$.
For the radius, which is $\sqrt{(\frac{1}{2}(x_2 - x_1))^2 + (\frac{1}{2}(y_2 - y_1))^2}$,
we can store the square of the diameter instead, which is $(x_2 - x_1)^2 + (y_2 - y_1)^2$.

# Code

An implementation in Rust is provided below.

```rust
use std::collections::HashMap;

#[derive(Eq, PartialEq, Hash)]
struct Circle {
    // we're using the trick from above 
    // to store center and radius in integer form
    fake_center: (i32, i32),
    fake_radius: i32
}

fn count_rect(list: &[(i32, i32)]) -> u32 {
    // circle -> # of diagonals on that circle
    let mut map = HashMap::<Circle, u32>::new(); 

    // loop through unordered pairs of points (diagonals)
    for (i, &(x1, y1)) in list.iter().enumerate() {
        for &(x2, y2) in list.iter().skip(i) {
            let circle = Circle {
                fake_center: (x1 + x2, y1 + y2),
                fake_radius: (x2 - x1).pow(2) + (y2 - y1).pow(2)
            };
            *map.entry(circle).or_default() += 1;
        }
    }

    // sum count * (count - 1) / 2 for each circle
    return map.into_iter()
        .map(|(_, count)| count * (count - 1) / 2)
        .sum();
}
```

<style>
.image-circle .img-wrapper {
    display: inline-block;
}
</style>
