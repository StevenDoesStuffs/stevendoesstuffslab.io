---
title: "Segment Trees"
date: 2020-06-20
draft: false
markup: md
tags: ["computer-science", "data-structures"]
author: "Steven Xu"
---

Before we start, note that **I'll be using inclusive starts and exclusive ends for intervals throughout the entire article**.

A segment tree is a data structure which allows for quickly querying arbitrary intervals of a list under some associative accumulator operation, 
like addition or maximum, and to be able to update elements in that list quickly as well. 
As such, segment trees usually have two methods: `query(start, end)`, `update(index, val)`. 
The goal is to get those operations in $O (\log n)$ time. 
Some implementations don't expose the underlying list to the user, and those will usually have a third method: `get(index)`.

<!--more-->

Here's what each method does: 

- `get(index)` simply returns the element at that index.
- `query(start, end)` returns the **accumulation**/**fold** (_remember those terms, we'll be using them_) of the interval starting at `start` and ending at `end`. 
The meaning of that changes depending on what the specified operation is.
If it was addition, we'd be returning the sum of the elements in the interval, 
but if it was maximum, we'd be returning the maximum element in the interval instead. 
- `update(index, val)` sets the value of the list at `index` to `val`. 

Here's an example to clear things up. 
Let's start with a list: `[5, 7, 2, 4, 6, 1, 6, 3, 8]`, and let our operation be maximum.
Consider this code:
```none
get(3)
query(3, 6)
update(1, 2)
query(1, 4)
```
- Line 1 simply asks for the element at index `3`, which is `4`.
- Line 2 queries the maximum element in the interval between between indices 3 and 6, which is `[4, 6, 1]`. The maximum element in there is `6`.
- Line 3 sets the element at index `1` to the value `2`. This replaces the `7` originally at that index, and our list is now `[5, 1, 2, 4, 6, 1, 6, 3, 8]`.
- Line 4 line asks for the maximum value between indices 1 and 4 (`[1, 2, 4]`), which is `4`.

As a side note, if the operation is invertible, like addition (but unlike maximum), then it's probably quicker to use the more efficient Binary Indexed Tree. 
Before we continue, ask yourself, why don't we use a prefix sum in that case instead? Hint: think about the runtime of the `update` method. 

## Construction

Segment trees are built recursively from the top down, and their nodes represent intervals of the list. 
Each node stores the fold of its respective interval, which we'll call its "value".

To build a node representing an interval, we split its interval in half, construct the segment tree for the left half and right half,
and then connect them with a middle node. The base case occurs when the interval is of length one, 
at which point we build a single node whose value is equal to that of its corresponding element (the single element inside its interval).

The new node's value is then `a * b`, where `a` is the value of the left node, `b` is the value of the right node, and `*` is the accumulation operator. 
The root node of a segment tree represents the entire list, and the leaves of a segment tree represents individual elements.

Here's what that process looks like. We'll use the same list as the previous example: `[5, 7, 2, 4, 6, 1, 6, 3, 8]`, and we'll use the same operation (maximum) as well.

First, we split the list in half. If the list has an odd number of elements, we'll put the middle element with the left group (this choice is arbitrary).
![Split the list in half](segment-tree/split-list.svg)

---

Then, we construct the segment tree recursively for both halves.
![Construct the segment tree recursively for both halves](segment-tree/build-halves.svg)

---

Finally, calculate the value of the middle node: $\max(7,8) = 8$, and connect it to the two halves. 
![Connect the two halves with a middle node](segment-tree/total.svg)

---

Each node represents the interval consisting of all the leaves beneath it. 
For example, the node highlighted in blue below represents the interval between indices 0 and 4.
![Each Node Represents an Interval](segment-tree/interval.svg)

## Length and Size

Before we continue, we need to clear up some terminology. The "length" of a segment tree is the length of the list it's built over,
whereas the the "size" of the segment tree is the number of node it contains. 

For any arbitrary segment tree, let $n$ be the length of that segment tree, 
$\text{size}(n)$ be the size of that segment tree, and $\text{height}(n)$ be the height of that segment tree. 
$\text{size}(n)$ and $\text{height}(n)$ are well defined since the size and the height (and the entire structure)
of a segment tree _only_ depends on its length. 

One neat property of a segment tree is that the height of the tree is $O(\log n)$.
A quick and informal proof is that when is $n = 2^k$, $height(n) = k + 1$. 
Then, since for all $m < n$, $\text{height}(m) \le \text{height}(n)$, the height is $O(\log n)$.

Also, the number of nodes is exactly $2n - 1$. We can prove this using induction. 
Clearly, $\text{size}(1) = 1 = 2(1) - 1$.
Now, assume that for all $m < n$, $\text{size}(m) = 2m - 1$. 

Consider the case where n is even. When we are constructing the segment tree, 
we construct two segment trees of length $ \frac{n}{2} $, and then connect them with another node. Then 
$$\text{size}(n) = 1 + 2(\text{size}(\frac{n}{2})) = 1 + 2(2(\frac{n}{2}) - 1) = 2n - 1$$
For when n is odd, we construct two segment trees, one of length 
$\lfloor \frac{n}{2} \rfloor$ and another of length $\lceil \frac{n}{2} \rceil$, 
and then connect them with another node. Then 
$$\text{size}(n) = 1 + \text{size}(\lfloor \frac{n}{2} \rfloor) + \text{size}(\lceil \frac{n}{2} \rceil) = 
1 + 2(\lfloor \frac{n}{2} \rfloor + \lceil \frac{n}{2} \rceil) - 2 = 2n - 1$$

Therefore, for any length $n$, $\text{size}(n) = 2n - 1$. 
This also means that construction time and storage space are both $O(n)$.

## Methods

### Query

Recall that `query(i, j)` returns the fold of the interval defined by `i` and `j`. 
The query algorithm starts at the root, and recursively traverses down the tree. 
This means we'll need to keep track of what node we're at, 
and so we'll define a new function `query_internal(node, i, j)`, and `query(i, j) = query_internal(root, i, j)`.
At each node, the `query_internal` does one of three things: 
1. If the node interval is completely contained _by_ the query interval, return the current node's value (base case). We call base case nodes the "leaves" of the traversal. 
2. If the query interval only has parts in _either_ the left or the right node's interval, recurse on that node and return its value.
3. If the query interval has parts in both the left _and_ the right node's interval, then recurse on both sides and return the accumulation of the two values.

It's fairly easy to see that the algorithm is really just calculating the fold of all the leaves of the traversal (not of the tree). 

Time for an example. Let's run `query(1, 7)` on the segment tree we just built, 
which should return the maximum of all the elements between index `1` and `7`.
Green marks the nodes we're currently at, blue marks the leaves as specified in the base case, and orange marks nodes we've visited. 
For convenience, I've also labeled each node with a letter. 
As usual, we start at the root. 

![Query at Depth 1](segment-tree/query-1.svg)
Our interval has parts in both the left and the right node of the root (case 3), so we recurse on both sides.

---

![Query at Depth 2](segment-tree/query-2.svg)
- Node B: The query interval has parts in both left and right parts (case 3), so we again recurse on both sides.
- Node C: The interval only has parts in its left child (case 2), so we recurse on that node only.

---

![Query at Depth 3](segment-tree/query-3.svg)
- Node D: Case 3
- Node E, F: The node's interval is completely contained in the query interval (base case), so it's a leaf node. 

---

![Query at Depth 4](segment-tree/query-4.svg)
- Node H: Case 2 (on the right)
- Node I: Base case

---

![Query at Depth 5](segment-tree/query-5.svg)
- Node Q: Base case

---

![Query at Depth 6](segment-tree/query-6.svg)
The fold of all the leaves is then `max(7, 2, 6, 6) = 7`, which indeed is the maximum element in the interval `1..7`. 

---

At first glance, it's not exactly clear why this algorithm is $O(\log n)$. 
After all, it traverses both children of some nodes, so what's to say there isn't a case where it traverses the entire tree?
As it turns out, we can actually prove that the query algorithm will traverse at most 4 nodes in any level of the tree. 
Since the height of the tree is $O(\log n)$, the performance of the algorithm is then also $O(\log n)$. 

We can prove this by induction. For our base case, there is only one node on the first level, 
the root node, so `query` cannot traverse more than four nodes there. 
Let `$t_k$` be the number of nodes traversed, and assume `$t_{k - 1} \le 4$`.
- If `$t_{k - 1} = 1$`, then at worst, the algorithm could decide to traverse both children of that single node, so `$t_k \le 2$`. 
- If `$t_{k - 1} = 2$`, then similar logic applies, and `$t_k \le 4$`. 
<p></p>

Now for the interesting cases. 

![Query Performance](segment-tree/query-perf.svg)
As the picture demonstrates, if `$t_{k - 1} = 3$` or `$t_{k - 1} = 4$`, then the middle nodes _must_ be leaves, 
since they must be completely contained inside the query interval.
This means that there are only two nodes which can recurse, and in the worst case, they both recurse on both their children.

Therefore, `$t_k \le 4$` in all four cases, and by induction, in general. 

### Update

Recall that `update(i, val)` sets the value of the list at index `i` to `val`.
Of course, we also have to update the tree to be able to accurately do queries later on.
Updating the tree is fairly simple. We start from the leaf node corresponding to the index,
update the value of that node, then update the value of all its parents until we get to the root.

Let's run `update(1, 2)`. Note that the list before we start is `[5, 7, 2, 4, 6, 1, 6, 3, 8]` 
and the tree look like so:
![Segment Tree Before Update](segment-tree/total.svg)

---

First, we update the array and the leaf node.
![Segment Tree After Updating Array and Leaf Node](segment-tree/update-1.svg)

---

Then, we update all the parents of that node, 
using the rule that each node's value is the fold of its left and right child.
![Segment Tree After Update](segment-tree/update-2.svg)

---

Because the height is $O(\log n)$ and we visit exactly one node per level, the runtime of this method is $O(\log n)$.
