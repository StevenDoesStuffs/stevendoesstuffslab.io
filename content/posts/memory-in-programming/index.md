---
title: "Memory in Programming"
date: 2019-09-28
draft: false
markup: md
tags: ["computer-science"]
author: "Steven Xu"
---

Nowadays, most of memory is abstracted away, with most developers no longer considering what and where the data in memory is, how it's laid out, and when we get free it. 
Even so, it's always good to understand things work under the hood. 
This article aims to shine some light on how memory acts in our programs and how some popular abstractions over memory, like garbage collection, work. 

<!--more-->

Memory acts like a long line of bytes. Since it's a line, it makes sense to talk about the `n`th byte, which is what pointers are. 
A pointer/memory address `ptr` points to the "`ptr`th" byte in memory. Dereferencing a pointer means to go to that memory address. 
In programming, we deal with random access memory, or RAM, which allows us to quickly access any byte of memory. 
This means reading/writing the 7th then the 23rd byte is approximately the same speed as reading/writing the 7th then the 8th byte.

# Notation

Take a look at this code and how it's laid out in memory: 

```rust
let a: u16 = 4; // u16 is a 16 bit unsigned number type
let b = ['a', 'c']; // assume each character is one byte
```
```none
stack
┌─────┬─────┬──────┬──────┐
│  a  │  a  │ b[0] │ b[1] │
└─────┴─────┴──────┴──────┘
```

The first two cells contain `a`, which is two bytes, the next cell contains `b[0]`, and the last cell contains `b[1]`. Sometimes I'll also draw it like so: 

```none
stack
┌──────┬──────┬──────┐
│ a{2} │ b[0] │ b[1] │
└──────┴──────┴──────┘

OR

stack
┌──────┬──────┐
│ a{2} │ b{2} │
└──────┴──────┘

OR 

stack
┌───────┬───────┐
│ a{2B} │ b{2B} │
└───────┴───────┘
```

Note: all examples will be on a 64 bit computer which means pointers and `usize`s will be 64 bit/8 bytes.

# The Stack and the Heap

When programming, there are two "types" of memory that we deal with, the stack and the heap. Both belong in RAM. 

## The Stack 

The stack is a small, continuous, and fixed size block of memory that gets allocated when the thread (program) starts. 
For example, for Rust, the stack is 2MB large. 
The size of everything that goes on the stack must be known at compile time (a restriction imposed by most languages). 

Variables go on the stack in the order that they're declared as the code runs.

```rust
struct Test {
    a: u32,
    b: u8
}

fn f(x: u32) -> u32 {
    let h = x + x;
    return h;
}

fn main() {
    let x: u64 = 79; // line 1
    let test = Test{a: 3, b: 4}; // line 2
    let y = f(x); // line 3
}
```

When the program evaluates line 1, the stack contains just `x`.

```none
┌────────────┐
│ x = 79 {8} │
└────────────┘
```

---

Once we evaluate line 2, the stack contains `x` and `test`.

```none
┌────────────┬─────────────────────────┐
│ x = 79 {8} │ test = {a: 3, b: 4} {3} │
└────────────┴─────────────────────────┘
```

---

When we start evaluating `f(x)` at line 3, we copy the parameter onto the top of the stack, 
and any variables created by that function go on top of the parameters. 

```none
                                        Parameter of 
                                        f(x)
┌────────────┬─────────────────────────┬────────────┐
│ x = 79 {8} │ test = {a: 3, b: 4} {3} │ x = 79 {8} │
└────────────┴─────────────────────────┴────────────┘
```

---

Everything from the parameters onwards is called the "stack frame" of a function.

```none
                                        Parameter of 
                                        f(x)
┌────────────┬─────────────────────────┬────────────┬─────────────┐
│ x = 79 {8} │ test = {a: 3, b: 4} {3} │ x = 79 {8} │ h = 158 {8} │
└────────────┴─────────────────────────┴────────────┴─────────────┘
                                       └────stack frame of f(x)───┘
└──────────────────────stack frame of main()──────────────────────┘
```

---

When the function finishes evaluating, its stack frame is popped from stack and its result is returned.

```none
┌────────────┬─────────────────────────┬─────────────┐
│ x = 79 {8} │ test = {a: 3, b: 4} {3} │ y = 158 {8} │
└────────────┴─────────────────────────┴─────────────┘
```

Note that when we talk about "freeing" stack memory, we're really just talking about popping memory from the stack. 
The memory where the stack element was is still owned by the stack; it doesn't go back to the OS; this is what makes the stack fixed sized. 
Obviously, we can only free from the top of the stack. Also, the stack isn't implemented the normal way a stack is implemented (a linked list); 
instead, it's more like an array, where the elements are all right next to each other in memory, 
and a counter somewhere tells us how much stuff there is on the stack. 
Popping an element is as simple as decreasing that counter, 
and pushing an element works by writing the data on top of the used portion of the stack and increasing the counter. 

In reality, the stack is much more complex, storing things such as the addresses of instructions, 
the `this` pointer in object oriented languages, and various other things. 

## The Heap

The heap is large and discontinuous. We ask for heap memory from the OS as the program runs which then gives us a memory address to a continuous block of memory of the requested size. 
The process of requesting and getting memory from the OS is called allocation since the OS is "allocating" a block of memory to your program. 
This process is slow because it requires the OS to go look for a continuous block of unused memory that is as large as you requested. 

The heap is discontinuous since there are multiple programs requesting memory from the OS. For example, imagine program A requests 1MB of heap memory. 

```none
┌────────────────┬──
│ program A {1M} │ ...
└────────────────┴──
```

--- 

Then program B requests 2MB of heap memory which the OS places like so. 

```none
┌────────────────┬────────────────┬──
│ program A {1M} │ program B {2M} │ ...
└────────────────┴────────────────┴──
```

Then when program A requests another 3MB, the OS can't allocate it the memory adjacent to its previous block since it's being used by another program. 
Note that it could be another program's stack memory next to your program's heap memory—there's no reason that's not possible. 

Also, the OS prevents your program from accessing memory that isn't allocated to you. 
Segmentation faults (segfaults) occur when trying to read or write to memory that isn't allocated to your program, and it's basically a crash. 

You can also free/deallocate memory which you'll no longer need while your program is running, returning that memory to the OS to allocate to some other program. 

# Data Structures

## Vector / ArrayList

There are two components to data structures with sizes that aren't known at compile time. 
The first component is the fixed size part that belongs on the stack which contains a pointer to memory on the heap, 
and the other component is the heap memory (buffer) which the pointer points to. 
The first component is why we're able to store such data structures in variables.

```rust
// not the actual implementation in std
struct Vec<T> {
    len: usize, 
    // Box<[T]> is a pointer to a [T] on the heap
    // [T] is that "second component" of our vector
    ptr: Box<[T]> 
}
```
```none
stack
┌────────┬────────┐
│ len{8} │ ptr{8} │
└────────┴────────┘
             │
    ┌────<───┘
┌──────┬──────┬──────┬──────┬──
│ T{?} │ T{?} │ T{?} │ T{?} │ ..
└──────┴──────┴──────┴──────┴──
^ heap
```

Notice that we don't have to use all our entire buffer. If we have 4 elements but the buffer is large enough to contain 8, that's fine. 

### Getting / Setting

Getting and setting variables in a vector is very easy. 

```rust
let mut vec: Vec<u32> = vec![7, 3, 4, 2];
```
```none
stack
┌─────────────┬────────────────┐
│ len = 5 {8} │ ptr = 1000 {8} │
└─────────────┴────────────────┘
                       │
    ┌─────────<────────┘
┌──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 4{4} │ 2{4} │
└──────┴──────┴──────┴──────┘
^ heap
```

---

```rust
let b = vec.get(3);
```

We know that the address of the first byte (the start of the 0th element) of the vector buffer is `1000`, the size of each element is 4 bytes, 
and since we're trying to get the third element, the address of that element is `1000 + 3 * 4 = 1012`. 
We then read the 4 bytes that make up the third element and copy them onto the stack. 
Notice how the only reason we're able to access the `n`th element in constant time is because the size of all the elements are the same, 
allowing us to do pointer arithmetic the way we just did. 

```none
stack
┌─────────────┬────────────────┬───────────┐
│ len = 5 {8} │ ptr = 1000 {8} │ b = 2 {4} │
└─────────────┴────────────────┴───────────┘
                       │
    ┌─────────<────────┘
┌──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 4{4} │ 2{4} │
└──────┴──────┴──────┴──────┘
^ heap
```

Setting an element works the same way.

### Pushing

Obviously, our buffer is finite, so what happens when we try to push an element to the end of a vector? 
Sometimes, we aren't using the entire block we allocated, 
in which case we just override the memory after the last element and increment the length counter, but sometimes we _are_ using the entire block. 

We can't just extend the buffer we have, since there might be some other program's memory after it, so we must obtain a larger buffer, 
copy the old data over, and _then_ push the new element onto the vector and free the old buffer. 
Asking the OS to allocate us another buffer, moving our data to the new buffer, and then freeing the old buffer is called reallocation. 

Here's how it looks when we declare a vector with 4 `u32`s in it. 

```rust
let mut vec: Vec<u32> = vec![7, 3, 4, 2];
```
```none
stack
┌─────────────┬────────────────┐
│ len = 4 {8} │ ptr = 1000 {8} │
└─────────────┴────────────────┘
                       │
    ┌─────────<────────┘
┌──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 4{4} │ 2{4} │
└──────┴──────┴──────┴──────┘
^ heap
```

---

Then we push the element `5` to the vector.

```rust
vec.push(5);
```

First, we increment the length and request new buffer that's large enough to contain our current data and one new element.

```none
stack
┌─────────────┬────────────────┐
│ len = 5 {8} │ ptr = 1000 {8} │
└─────────────┴────────────────┘
                       │
    ┌─────────<────────┘
┌──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 4{4} │ 2{4} │
└──────┴──────┴──────┴──────┘
^ heap

heap (address = 2000)
┌──────┬──────┬──────┬──────┬──────┐
│ ?{4} │ ?{4} │ ?{4} │ ?{4} │ ?{4} │
└──────┴──────┴──────┴──────┴──────┘
```

---

Second, we copy the old data over. 

```none
stack
┌─────────────┬────────────────┐
│ len = 5 {8} │ ptr = 1000 {8} │
└─────────────┴────────────────┘
                       │
    ┌─────────<────────┘
┌──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 4{4} │ 2{4} │
└──────┴──────┴──────┴──────┘
^ heap

heap (address = 2000)
┌──────┬──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 4{4} │ 2{4} │ ?{4} │
└──────┴──────┴──────┴──────┴──────┘
```

---

Third, we push the last element onto the new buffer. 

```none
stack
┌─────────────┬────────────────┐
│ len = 5 {8} │ ptr = 1000 {8} │
└─────────────┴────────────────┘
                       │
    ┌─────────<────────┘
┌──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 4{4} │ 2{4} │
└──────┴──────┴──────┴──────┘
^ heap

heap (address = 2000)
┌──────┬──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 4{4} │ 2{4} │ 5{4} │
└──────┴──────┴──────┴──────┴──────┘
```

---

Finally, we change the pointer in the stack variable and free the old buffer. 

```none
stack
┌─────────────┬────────────────┐
│ len = 5 {8} │ ptr = 2000 {8} │
└─────────────┴────────────────┘
                       │
    ┌─────────<────────┘
┌──────┬──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 4{4} │ 2{4} │ 5{4} │
└──────┴──────┴──────┴──────┴──────┘
```

Reallocating the vector every time we need to add an element is really bad for performance (it makes insert $O(n)$), which is why we use tricks to minimize how often we have to do it, 
for example, by requesting enough memory for `2n` elements instead of `n + 1` elements every time we need to expand the vector. 
That specific way will actually make insert run in amortized $O(1)$, which is much better.

### Inserting

For the sake of demonstration, we'll assume that our buffer is large enough to contain `n + 1` elements.

```rust
let mut vec: Vec<u32> = vec![7, 3, 4, 2];
```
```none
stack
┌─────────────┬────────────────┐
│ len = 4 {8} │ ptr = 1000 {8} │
└─────────────┴────────────────┘
                       │
    ┌─────────<────────┘
┌──────┬──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 4{4} │ 2{4} │ ?{4} │
└──────┴──────┴──────┴──────┴──────┘
^ heap
```

---

```rust
vec.insert(1, 5); // insert 5 into position 1
```

First, we have to move all the elements at `1` or beyond right by one. 

```none
stack
┌─────────────┬────────────────┐
│ len = 4 {8} │ ptr = 1000 {8} │
└─────────────┴────────────────┘
                       │
    ┌─────────<────────┘
┌──────┬──────┬──────┬──────┬──────┐
│ 7{4} │ 3{4} │ 3{4} │ 4{4} │ 2{4} │
└──────┴──────┴──────┴──────┴──────┘
^ heap
```

---

Then, we set element at `1` equal to `5`.

```none
stack
┌─────────────┬────────────────┐
│ len = 4 {8} │ ptr = 1000 {8} │
└─────────────┴────────────────┘
                       │
    ┌─────────<────────┘
┌──────┬──────┬──────┬──────┬──────┐
│ 7{4} │ 5{4} │ 3{4} │ 4{4} │ 2{4} │
└──────┴──────┴──────┴──────┴──────┘
^ heap
```

### Linked Lists 

```rust
// not the actual implementation in std
struct Node<T> {
    value: T,
    // Option means that the value might be null
    // Box<Node<T>> is a pointer to a Node<T> on the heap
    ptr: Option<Box<Node<T>>> 
}
```

---

```rust
let first = Node<u32> {
    value: 7,
    ptr: None
};
```
```none
stack
┌───────────────┬────────────────┐
│ value = 7 {4} │ ptr = None {8} │
└───────────────┴────────────────┘
```

For this section, we'll also write that like so:

```none
stack
┌───┬──────┐
│ 7 │ None │
└───┴──────┘
```

---

Linked lists are all over the place in memory. Here's what that might look like. 

```rust
let first: Node<u32> = linked_list![7, 3, 4, 2];
```
```none
stack
┌───┬────────────┐
│ 5 │ Some(2000) │──>──┐
└───┴────────────┘     │
                       │
heap                   │         ┌──────>─────┐
 ───┬───┬──────┬─   ─┬───┬────────────┬─   ─┬───┬────────────┬──
... │ 2 │ None │ ... │ 7 │ Some(2264) │ ... │ 4 │ Some(1276) │ ... 
 ───┴───┴──────┴─   ─┴───┴────────────┴─   ─┴───┴────────────┴──
      └────────────────────────<─────────────────────────┘
```

I'll leave it as an exercise for the reader to think about getting, setting, inserting, and pushing.

# Memory Management

Memory management is the way a language creates, places, and frees data in memory. 

## Java

Java's memory management system is the most complex, since it's a higher level language. 
Basically, Java primitives are stack variables, and all other variables exist as pointers on the stack to data on the heap. 
The garbage collector (GC) then frees any heap memory sometime after it's no longer needed. 
Recall that the data on the stack is automatically freed when the function that owns it finishes executing, meaning there is no need for garbage collection on the stack. 

```java
class Test {
    int i, j;
    // imagine a constructor here
}

// in the main method
int a = 0;
Integer x = 3;
Test test = new Test(5, 9);
```
```none
stack
┌───────────┬─────────────┬────────────────┐
│ a = 0 {4} │ x = ??? {8} │ test = ??? {8} │
└───────────┴─────────────┴────────────────┘
                   │               │
┌───────┐          │               │
│ 3 {4} │──────<───┘               │
└───────┘                          │
^ heap                             │
                                   │
┌─────────────────────┐            │
│ new Test(5, 9) {??} │──────<─────┘
└─────────────────────┘
^ also heap
```

### Java `ArrayList`s and Fake Generics

The reason Java's `ArrayList` can only contain objects and not primitives is actually because Java generics are _fake_. 
No matter what the `T` is `ArrayList<T>`, it's actually always an `ArrayList` of pointers. 

```java
ArrayList<Test> list = new ArrayList<>();
list.add(new Test(2, 5));
list.add(new Test(7, 23));
```
```none
stack
┌───────────┐
│ list {??} │────┐
└───────────┘    │
       ┌─────<───┘
heap   │
┌─────────────┬─────────────┬─────────
│ list[0] {8} │ list[1] {8} │ ... (unused buffer memory)
└─────────────┴─────────────┴─────────
       │             └─────────┐
heap   ↓                       │
┌─────────────────────┐        │
│ new Test(5, 9) {??} │        │
└─────────────────────┘        ↓
                               │
heap                           │
┌──────────────────────┐       │
│ new Test(7, 23) {??} │───────┘
└──────────────────────┘

```

### Garbage Collection

Java and most other GC'd languages have a "managed heap". 
This means that there is a runtime that will request a big block of heap memory from the OS, 
and any allocations that need to be done are done by the runtime onto the managed heap instead of by the OS. 
How the Java GC works is beyond the scope of this article, but we will take a look at what the GC does. 

Example 1:

```java
// method 1
out.println(/* test1 */ new Thing(1, 5)); 
runVeryLongProgram(/* test2 */ new Thing(3, 7));

// method 2
Test test3 = new Thing(1, 5);
out.println(test3); 
runVeryLongProgram(test3);
return test3;
```

Note: this may not be exactly accurate, but it does properly demonstrate the purpose of GC, and why memory management isn't exactly trivial.

Looking at method 1, we see that the object (data on the heap) `test1` only needs to live (not get freed) for the duration of the `println` method, 
and `test2` needs to live for the duration of the `runVeryLongProgram` method, 
but looking at method 2, we see that `test3` needs to live past its method since it gets returned. 
Garbage collectors run at runtime, and their job is to run periodically, checking if pieces of memory still needs to live, and freeing it if it doesn't. 

#### Simple GC Strategies

There are two simple strategies for GC. The first is called reference counting. 
Each piece of data on the heap counts the number of references pointing to it, 
and when the number of references becomes zero, the heap memory is freed. 
Unfortunately, this doesn't work very well for recursive data structures.

```java
class Test {
    Test other;
    // constructor
}

Test a = new Test(null);
Test b = new Test(a);
a.other = b;
```

Here, `a` has a reference pointing to `b`, and `b` has a reference pointing to `a`. 
As such, their reference counts would never reach zero, and even if the program no longer needs `a` or `b`, 
the fact that they point to each other prevents either object from being freed. 

The second simple strategy for GC is called tracing. 
The tracing GC creates a list of heap data that won't be freed, its contents determined by "tracing", and then frees everything not in that list. 
Tracing starts on the stack and adds all the references on the stack onto the list, 
all the references [contained within the heap data] that those references point to, 
and all the references that _those_ references point to, etc. 
Most GCs are a variation of this strategy but with various improvements, such as the usage of "generations".

## Rust

Rust's memory management system is less abstracted away from the programmer; it uses "declarative memory management". 
Basically, while you don't have to manually free and allocate, Rust enforces ownership rules that tells it when to free certain memory on the heap. 
Since understanding how ownership works is so essential, they've documented it quite well, and as such, 
I'll just direct you [there](https://doc.rust-lang.org/stable/book/ch04-00-understanding-ownership.html) instead of rewriting it.
Their documentation also explains the stack and the heap really well. 

Note that one difference from Java is that `struct`s go on the stack, unlike objects in Java in which the data goes on the heap and a pointer goes on the stack. 

```rust
struct Test {
    a: u32,
    b: u8
}

fn main() {
    let test = Test{a: 3, b: 4};
}
```
```none
stack
┌───────────┬───────────┐
│ a = 3 {4} │ b = 4 {1} │
└───────────┴───────────┘
└───────────┬───────────┘
           test
```

## C/C++

In C, _you_ do the memory management. Sure, stack variables (like structs) still get dropped at the end of the function, 
but all heap memory stays until you manually free it (with the exception of destructors in C++).

First, let's take a look at static arrays.

```c++
int& test() {
    int foo[10];
    return foo[0];
}

int main()
{
    cout << test();
    return 0;
}
```

Here, we call `test()`, which declares `foo`, a static array on the stack, and returns a reference to `foo[0]`. 
We then try to dereference that reference when we write its value to stdout. 
This program segfaults because since `foo` is a stack variable; 
it gets dropped when `test()` finishes running, invalidating the reference it just returned.

---

Next, let's take a look at dynamic arrays.

```c++
int& test() {
    int* foo = new int[10];
    return foo[0];
}

int main()
{
    cout << test();
    return 0;
}
```

Here, we call `test()`, which declares `foo`, a dynamic array on the heap stored as a pointer on the stack, 
and returns a reference to `foo[0]`, which we then deference later. This program runs because unlike the previous case, 
the actual data of the array isn't dropped when `test()` finishes running since it exists on the heap instead of the stack; 
only the pointer to `foo` gets dropped; hence, the reference to `foo[0]` remains valid.

---

Finally, let's look at destructors, which are only available in C++.

```c++
int& test() {
    vector<int> foo;
    foo.push_back(1);
    foo.push_back(2);
    return foo[0];
}

int main()
{
    cout << test();
    return 0;
}
```

Here, we call `test()`, which declares `foo`, a vector with a destructor whose data exists on the heap, 
and returns a reference to `foo[0]`, which we then deference later. This program segfaults because unlike the previous case, 
the actual data of the `foo` _is_ dropped when `test()` finishes running since its destructor runs at the end of the function, freeing `foo`'s heap memory; 
this causes the reference to `foo[0]` to become invalid. 
Note that if you're trying to test this yourself, segfaults don't actually always occur; they're undefined behavior. 

Fortunately, the first two examples are more or less legacy code only used when writing in C and not C++. 
Nowadays, memory management in C++ is done with smart pointers, which can do things like reference counting and ownership, 
making life significantly safer and easier.
