---
title: "Visualizing Dijkstra's"
date: 2019-07-10
# draft: true

tags: ["algorithms", "computer-science"]
author: "Steven Xu"
markup: md
---

One of the most famous problems in computer science is the shortest path problem: given a graph and two nodes, find the shortest path between the two nodes. 
The SPP is usually solved using Dijkstra's algorithm which gives the shortest path between a single source and all other nodes and only works on graphs without negative edge weights. 
It's extremely similar in implementation to BFS with the only difference being that Dijkstra's uses a min-heap (priority queue) sorted by total distance. <!--more-->
A documented implementation in Rust is given below.

```rust
fn dijkstra<N>(graph: &Graph<N, u32>, start: Node) -> HashMap<Node, (Node, u32)> {
    // maps a node -> (parent node, total distance)
    let mut shortest_path: HashMap<Node, (Node, u32)> = HashMap::new();
    // min-heap of nodes sorted by total distance
    // we since BinaryHeap is by default a max-heap, we use Reverse to turn it into a min-heap
    let mut pq: BinaryHeap<Reverse<(u32, Node, Node)>> = BinaryHeap::new();
    // insert starting node
    pq.push(Reverse((0, start, start)));

    // keep looping until we have a sp for all nodes or we've explored everything
    while shortest_path.len() < graph.node_count() || !pq.is_empty() {
        // pop the smallest value from min-heap
        let Reverse((cost, node, prev_node)) = pq.pop().unwrap();

        // ignore longer paths to a given node
        if shortest_path.contains_key(&node) {continue}

        // this is the shortest path to node, insert into shortest_path
        shortest_path.insert(node, (prev_node, cost));

        // explore neighboring edges
        for edge in graph.edges_directed(node, Outgoing) {
            // if we already have the shortest path for target node, continue
            if shortest_path.contains_key(&edge.target()) {continue}
            // insert into min-heap if not
            pq.push(Reverse((cost + *edge.weight(), edge.target(), node)));
        }
    }

    shortest_path
}
```

So why does this work? To make the explanation easier, let's start off by exploring a slightly different algorithm, 
one that needs to mutate the graph to solve the single source SPP. 
The idea behind this algorithm is that the source node will continuously "suck" all adjacent nodes closer at the same rate. 
We'll call this algorithm the suck algorithm (yes I made that up). 

## The suck algorithm

---

![Graph after 0 iterations of the suck algorithm](graph/0.svg)
To start off, we have the original graph. `A` is the source node, and the shortest distance from `A` to `A` is 0. 
`A` is also its own parent node. We'll record that in a `HashMap` mapping from node to total distance (along the shortest path) and parent node.

| Node | Distance | Parent Node   |
|------|----------|---------------|
| A    | 0        | A             |

---

![Graph after 1 iteration of the suck algorithm](graph/1.svg)
**Note**: If an edge from `A` to `Q` (`AQ`) is labeled `W: n` for some node `W`, the edge was previously connecting `W` and `Q`, 
and if that edge is a part of the shortest path to `Q`, `W` will be the node directly before `Q` along that path, AKA the parent node. 
For example, in the picture above, there is an edge from `AD` with a label `G: 7`. 
This means that the edge was previously connecting `G` and `D` before `G` got consumed. 

After one iteration of the suck algorithm, `A` sucks in `G`, the closest node with a distance of `5`, and the edge `AG` disappears. 
The other neighbors of `A` (`C` and `F`), have their adjacent edge weights decremented by `5`, 
and the edges previously attached to `G` (`GD` and `GF`) are now attached to `A`.

| Node | Distance | Parent Node   |
|------|----------|---------------|
| A    | 0        | A             |
| G    | 5        | A             |

---

![Graph after 2 iterations of the suck algorithm](graph/2.svg)
After two iterations of the algorithm, `A` sucks in `F`, the closest node with a distance of `3`, 
using the path gained from consuming `G` (making `G` the parent node). 
The current other neighbors of `A` (`C`, `D`, and `E`) have their adjacent edge weights decremented by `3`, 
and the edges previously attached to `F` (`FE` and `FB`) are now attached to `A`. 
Since we've sucked a total distance of `5` before this iteration, the total distance from `A` to `F` is `5 + 3 = 8`. 

| Node | Distance | Parent Node   |
|------|----------|---------------|
| A    | 0        | A             |
| G    | 5        | A             |
| F    | 8        | G             |

---

![Graph after 3 iterations of the suck algorithm](graph/3.svg)
After three iterations of the algorithm, `A` sucks in `D`, the closest node with a distance of `4`, using the path gained from consuming `G`. 
The current other neighbors of `A` (`B`, `C`, and `E`) have their adjacent edge weights decremented by `4`, 
and the edges previously attached to `D` (`DC` and `DB`) are now attached to `A`. 
Since we've sucked a total distance of `8` before this iteration, the total distance from `A` to `D` is `8 + 4 = 12`. 

| Node | Distance | Parent Node   |
|------|----------|---------------|
| A    | 0        | A             |
| G    | 5        | A             |
| F    | 8        | G             |
| D    | 12       | G             |

---

The rest of the algorithm continues in a similar fashion: consume the closest node, decrement other neighboring nodes, 
append edges previously belonging to the consumed node, update the table and the total distance sucked, repeat until finished. 
Notice how `A` at each step "contains" all the nodes we have the shortest path for.

It's fairly easy to see why this algorithm works. For any graph, we immediately know the SP from the source node, 
`A`, to the closest node, `B`, since `B` is always the closest adjacent node, and the SP between the two is the edge between them. 
Because `B` is the closest node, we can move all nodes closer to `A` by `AB` and only consume `B`. 
Fortunately, moving all the nodes closer is quite easy, as it can be done by simply moving all the adjacent nodes closer. 
Once we do so, we produce a graph with a new closest node, and we can run the algorithm on this new graph and discover another node's shortest path. 

A more intuitive explanation would be that the suck algorithm is equivalent to dragging every node closer to `A`  via every path simultaneously at the same rate. 
Since a node gets consumed once any one of its paths to `A` get consumed, every node will get consumed by its shortest path. 

For use later, let's also write some code for this algorithm. 

## The immutable suck algorithm

It's not great that the suck algorithm eats the entire graph, so let's try to come up with a version that doesn't have to mutate the graph. 
We'll continue to use the `HashMap` to store information about the shortest path to a node. 
Also, we'll need to keep track of the edges we'd normally mutate. When the algorithm mutates an edge, 
it's either shrinking the edge weight of an edge already connected to `A`, removing the edge closest to `A`, or moving an edge `BC` to `AC`. 

The algorithm moves an edge `BC` to `AC` only during the process of consuming node `B`. After `B` is consumed, all edges pointing to `B` are now invalid. 
This makes it very easy to track the removal part of moving edges: an edge `XY` is is invalid iff `X` or `Y` has been consumed. 
As for the addition part of moving edges, we only ever need to add edges to `A`, which means we must use some collection to track the edges of `A`. 
Since we need to pop the closest edge to `A` at every iteration, we'll use a min-heap.

Unfortunately, we'll also need to mutate the edges already in the min-heap to "suck" all the adjacent nodes closer, 
and mutating values within a min-heap is quite difficult and inefficient, 
so let's examine the values of the min-heap at each step to see if we can avoid doing the mutation. 
Before the first iteration, the min-heap contains all the edges directly adjacent to `A`, which we'll denote as `[A]`. 
We pop the edge `AB` with distance `|AB|`, decrease all adjacent edge weights by `|AB|`, and then append the edges directly adjacent to `B`. 
After the first iteration, our heap looks like `[A - |AB|, B]`. 
Of course, the individual edges of `A - |AB|` and `B` are all mixed together such that they are sorted since we are using a min-heap. 
We'll refer to `A - |AB|` and `B` as terms, and the individual edges as elements.

Let `prev_dist(i) = |AB1| + |AB2| + ... + |AB{i}|` be the total distance traveled before the `i`th iteration, and let `D(i, x) = prev_dist(x) - prev_dist(i)`. 
After `x` iterations, our heap (which we'll call `heap(x)`) looks like `[A - D(1, x), B1 - D(2, x), ..., Bx - D(x, x)]` (take your time to fully understand why).
Now imagine what our heap looks like if we added `prev_dist(x)` to every element:  we get `[A + prev_dist(0), B1 + prev_dist(1), B2 + prev_dist(2), ..., Bx + prev_dist(x)]`. 
Note that this gives us a general formula for `heap(x) + prev_dist(x)`, which we'll refer to as `other_heap(x)`. 
Since the two heaps differ by a constant, the heaps are both ordered the same way: if we pop an element `AX` from `other_heap(x)`, 
we could calculate what element we would've gotten from `heap(x)` by subtracting off `prev_dist(x)` from `|AX|`. 

Using our formula, it's easy to see that `other_heap(x + 1) = [A + prev_dist(0), B1 + prev_dist(1), ..., Bx + prev_dist(x), B{x + 1} + prev_dist(x + 1)]`. 
The first `x` terms are the same as those of `other_heap(x)`, and the only difference is that we added the term `B{x + 1} + prev_dist(x + 1)`.
This means that if we modified our algorithm to use `other_heap(x)` instead of `heap(x)`, we don't have to mutate elements already in the heap.

Only one problem remains: whenever we consume a node, we must record its total distance (along the shortest path) and parent node. 
In the mutable version of the suck algorithm, we would keep track of how much distance we've sucked (`prev_dist(x)`) and add that to the edge weight of the edge we're consuming,
which was stored in the elements of the min-heap. We could do the same thing, but it turns out we don't need to. 
To pop an element from the `other_heap(x)` as if we were popping it from `heap(x)`, we pop the element, which we'll call `AX`, and we subtract `prev_dist(x)` from `|AX|`.
Then to calculate the total distance, we add back `prev_dist(x)` to `|AX|`. The two `prev_dist(x)`s cancel out, and it's like we never mutated `AX` at all.
So it turns out, `|AX|` is actually just the shortest distance from `A` to `X`. 

Finally, instead of starting the algorithm with the heap initialized with all the neighbors of `A` and the hashmap initialized `A -> (0, A)`, 
we can actually initialize the heap with a virtual edge pointing to `A` of distance 0.
Here's an implementation in Rust.

```rust
fn immutable_suck<N>(graph: &Graph<N, u32>, start: Node) -> 
HashMap<Node, (Node, u32)> {
    // maps a node -> (parent node, total distance)
    let mut shortest_path: HashMap<Node, (Node, u32)> = HashMap::new();
    // min-heap of nodes sorted by total distance (distance, node, parent node)
    // we since BinaryHeap is by default a max-heap, we use Reverse to turn it into a min-heap
    let mut min_heap: BinaryHeap<Reverse<(u32, Node, Node)>> = BinaryHeap::new();

    // insert virtual edge
    min_heap.push(Reverse((0, start, start)));

    // keep looping until we have a sp for all edges or we're out of edges
    for i in 0..graph.node_count() {
        if min_heap.is_empty() {break}
        // pop the closest edge value from min-heap
        let Reverse((cost, node, prev_node)) = min_heap.pop().unwrap();

        // ignore edges whose target node has already been consumed/we already have the shortest path for
        if shortest_path.contains_key(&node) {continue}

        // record shortest path, consuming the node
        shortest_path.insert(node, (prev_node, cost));

        // move node's neighbors
        for edge in graph.edges_directed(node, Outgoing) {
            // ignore edges whose target node has already been consumed/we already have the shortest path for
            if shortest_path.contains_key(&edge.target()) {continue}
            // move edges  
            min_heap.push(Reverse((cost + *edge.weight(), edge.target(), node)));
        }
    }

    shortest_path
}
```

As it turns out, the immutable implementation of the suck algorithm is simply Dijkstra's shortest path algorithm! 
And now, you have a neat way of proving/visualizing Dijkstra's algorithm.
