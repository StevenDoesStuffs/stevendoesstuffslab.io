---
title: "Fast Resizing for Robin Hood Hashing"
date: 2020-07-26
draft: false
markup: md
tags: ["computer-science", "data-structures", "algorithms"]
author: "Steven Xu"
---

A while ago, my friends and I hosted a competition to see who can create the fastest hashtable. 
Ultimately, I lost, but I ended up discovering a fast resizing algorithm for robin hood hashing which I want to share here today.
The "inspiration" for this algorithm comes from [here](https://github.com/rust-lang/rust/blob/379c380a60e7b3adb6c6f595222cbfa2d9160a20/src/libstd/collections/hash/map.rs#L139),
but unfortunately, the source material doesn't actually contain much. 
If you don't know what hashtables are or how robin hood hashing works, you can read a "part 1" [here](/posts/intro-hashtables-robin-hood-hashing).

<!--more-->

# New Perspective on Robin Hood

In order to see how robin hood hashing really works, we have to again change our illustration. 
Instead of displaying the distance from the home bucket, I'll display the home bucket itself. 
In general, I'm going to swap out rows depending on the illustration throughout the article. 
Here's what the hashtable looks like after all five inserts with robin hood:
![Home Buckets Displayed](new-perspective/robin-hood.svg)

The only entry sitting on its home bucket is the entry in bucket 5. 
If we start there and list all the homes of each of the succeeding entries until we reach another at-home entry or an empty bucket, we'll get `5 5 0 0 1`.
Notice how we get all the `5`s next to each other together, then the `0`s, and then the `1`s. As it turns out, robin hood hashing always organizes its entries this way.

1. The home bucket of every entry in the hashtable is occupied.
2. All entries are partitioned into "chains" such that each entry is in the same chain as the entry in its home bucket. 
   All chains must be continuous (keep in mind that the hashtable is circular), 
   and the first entry in any chain is the one and only entry in its own home bucket within that chain. 
   We call that entry the head of the chain. 
3. Within those chains, continuous blocks of entries that share the same home bucket are called clusters. 
   All clusters within a chain are ordered from least to greatest by the distance between the start of the chain and the cluster's home bucket. 

I'll be referring to the three rules as P1, P2, and P3 respectively throughout the post. 
Before we continue, I'd like to point out two neat results.
First, _all_ the entries that have the same home bucket are in the same cluster. 

Second, for any possible set of entries, there is only one valid hashtable that satisfies all three properties if we're only looking at the home buckets of the entries.
For example, if we have two entries with a home bucket of `3`, one entry with a home bucket of `0`, and one entry with a home bucket of `4`, 
the only valid hashtable is `0 _ _ 3 3 4`. This also means that robin hood insert is order independent. No matter what order we inserted those entries in, 
we would've always gotten `0 _ _ 3 3 4` when we looked at the home buckets of all of the entries.

Anyhow, in our example from before, all entries are in the same chain. The picture below shows our example but with a line connecting each entry to its home bucket. 
![Connecting Lines](new-perspective/lines-1.svg)
Notice that if entries `a` and `b` are in the same chain and `b` and `c` are also in the same chain, 
then `a` and `c` are also in the same chain (this is called the transitive property of equivalence relations). 
This means that if there's a path between two entries using the lines, then they're in the same chain. 

The clusters are `5 5`, `0 0`, and `1` in that order since the distance between `5` (bucket of the head) and `5` (home bucket of the first cluster) is 0,
the distance between `5` and `0` (home bucket of the second cluster) is 1, and the distance between `5` and `1` is 2. 

Let's take a look at another example. 
![New Example of Chains](new-perspective/lines-2.svg)
This time, we have two chains, one of which is colored blue and the other colored orange. 
In the first chain, the only cluster is `0 0`. 
In the second chain, the two clusters are `3 3` and `4`. 

Before we move on, I'd like to point out that two chains can be adjacent, meaning not every continuous block of entries is a chain.
![New Example of Chains](new-perspective/lines-3.svg)
In the example above, the entry with home bucket `3` is in its own chain since it resides in its home bucket. 

## Analysis of Insert

At this point, it's natural to ask why the three rules are true. Fortunately, we can prove them with induction. 
For the base case, an empty hashtable obviously satisfies all three properties. 

From there, let's assume that we have a hashtable which satisfies the properties, and let's try inserting an entry. 
If the entry's home bucket is empty, then we insert it there and satisfy P1 (the first property). 
Since that entry is the only entry in its chain P2 and P3 hold as well. 

If the entry's home bucket isn't empty, we'll be trying to insert it into an existing chain. 
Let's take a second to recall how inserting works. 
We start at the home bucket of the inserting entry and move forward until we find an empty bucket.
During the process, if we visit a bucket where the inserting distance is greater than the occupying distance, we swap the inserting entry and the occupying entry. 

Let's think about when that happens. If the inserting distance is equal to the occupying distance, 
then the home buckets of the inserting entry and the occupying entry are equal. 
Note that there's exactly one cluster of these entries within our chain. 
Since the clusters are ordered and the inserting distance only increases in between the swaps, 
the first bucket of the next cluster is when the first swap happens.
We can then apply that logic again after we swap and see that a swap happens at the start of every cluster after the inserting cluster.

For example, if we're inserting an entry with home bucket `1`, then the first swap happens right after the `1` cluster. 
(In the picture below, `idist` means inserting distance and, `odist` means occupying distance.)
![Insert Analysis Part 1](new-perspective/dist-cmp-1.svg)

---

We do the swap (now inserting an entry with home bucket `2`), and then we can see that we have to do another swap at the start of the next cluster. 
![Insert Analysis Part 2](new-perspective/dist-cmp-2.svg)

---

We do that swap (now inserting an entry with home bucket `3`), and then we put that entry into the next empty bucket. 
![Insert Analysis Part 3](new-perspective/dist-cmp-3.svg)

---

Looking at the result, we see that what robin hood hashing really does is move everything 
after the inserting cluster and before the next empty bucket over by 1, and then put the inserting entry into the empty space created. 
This is because the first entry in any post-inserting cluster we visit during insert always gets moved to right after the end of its cluster,
which is equivalent to moving the cluster over by one. 
![Insert Analysis Part 4](new-perspective/copy-shift.svg)
Also, this gives rise to a very simple deletion algorithm—clear the bucket of the entry we're deleting, 
then move everything in the current chain backward by one. 

---

From here, it's clear that robin hood insert does preserve all three properties.
Remember that we're only considering the case where the inserting entry's home bucket is occupied. 
- For P1, no bucket that was previously filled is empty after an insert. 
- For P2, the way we shift everything over only ever increases the probing distance, so no entry is shifted onto its home bucket.
  Also, we can never shift the first cluster of the inserting chain, so the first entry remains in its home. 
  Sometimes there is no empty space directly after our chain, so when we do the shift, we shift other chains as well,
  knocking the head of those chain of its home bucket. 
  Fortunately, that's okay because it just means those chains got merged into the inserting chain, and P2 still holds. 
- For P3, the place where we put our inserting entry is right at the end of the cluster of the same home bucket 
  (or where that cluster would be if it didn't already exist). Therefore, we maintain the cluster ordering. 
  In the case where we end up merging other chain(s), 
  we still preserve P3 since the clusters in those chains are ordered and the chains themselves are ordered. 

# Fast Resizing Algorithm

Finally, I'd like to cover the fast resizing algorithm I mentioned at the very beginning. 
I'll be referring to the pre-resize hashtable as the old hashtable and the post-resize hashtable as the new hashtable. 
The point of the resize algorithm is to copy over all the entries from the old hashtable to the new hashtable in a way that makes it a valid robin hood hashtable. 
The fast resize algorithm only works when the length of the new hashtable is double the length of the old hashtable (which we'll call `n`). 
This means an entry with home bucket is `i` before the resize will either have a home bucket of `i` or `i + n` after the resize 
since if $h = i \pmod {n}$ (where $h$ is the hash of the entry), then either $h = i \pmod{2n}$ or $h = i + n \pmod{2n}$.

Anyhow, the first thing we have to do is to find the chain head closest to the bucket `0` (moving forward only). 
Let's call the bucket of that entry `s`. From there, we visually move everything before it to the end like in the picture below. 
![Visual Shift + New Hashtable](fast-resizing/visual-shift.svg)
What that does is make it so that no chain loops around from the end back to the beginning, and we start the filled portion of the hashtable on a chain head. 
Eventually, we'll wanna copy all our elements from the old hashtable over to the new one (represented by the third row). 

First, notice that we're representing entries by their home bucket in the new hashtable. 
Second, after we move, if we are allowed to add or subtract `n` from those home buckets, we could get the table above to be a valid robin hood hashtable. 
It would look like `2 3 3 4 5 6`. Notice that property also holds in the first row (before the move). 

Anyhow, we can conceptually split the new hashtable into two parts. 
The primary branch are the buckets in the range `s..(s + n)` (we're using exclusive ends, so `s + n` itself isn't included in the range). 
The secondary branch are the buckets in `(s + n)..s` (as usual, the end wraps back to the beginning like in a circle).
Both these branches are continuous blocks of buckets, and they're shown for our example below. 
![Branches in the New Hashtable](fast-resizing/branches.svg)

One neat thing about the branches is that once we copy things over to the new hashtable, every entry will be in the same branch as its home bucket. 
That's because one way of copying the old hashtable to the new hashtable is to copying the entire old hashtable into both branches of the new one, 
removing all the entries that are in the wrong branch, and shifting every entry as close as possible to its home bucket. 
It's not too hard to see that that process produces a hashtable which has all the entries and is a valid robin hood hashtable (follows the three rules). 
The picture below shows the process above for our example. 
![Copy and Remove](fast-resizing/copy-remove.svg)
The point of the fast resizing algorithm is to do that but more efficiently. 

The way we do this is first, we have to keep track of the latest inserted bucket in both branches 
(which we'll call `l1` and `l2` for the primary and secondary branches respectively). 
Then, we loop through all the entries in our old hashtable. 
At each step, if the entry in question has an empty home bucket in the new hashtable, insert it there. 
If not, insert it at the bucket right after the latest filled bucket in its respective branch. 

Here's an example. 

We start with the first entry in our old table in bucket `2`. 
Its home bucket, `2`, is empty in the new hashtable, so we insert it there. 
![Copy Part 1](fast-resizing/copy-1.svg)

---

We then move onto bucket `3`, whose entry has a home bucket of `9`, which is empty, so we insert it there. 
![Copy Part 2](fast-resizing/copy-2.svg)

---

At bucket `4`, we have an entry with a home bucket of `9`, which is occupied and in the secondary branch, 
so we insert it after the latest filled bucket in the that branch, in bucket `10`. 
![Copy Part 3](fast-resizing/copy-3.svg)

---

Buckets `5`, `6`, and `7`, all have home buckets which are empty after the previous operations like in buckets `2` and `3`, 
so we won't go over them in detail.

Bucket 5: 
![Copy Part 4](fast-resizing/copy-4.svg)

---

Bucket 6: 
![Copy Part 5](fast-resizing/copy-5.svg)

---

Bucket 7: 
![Copy Part 6](fast-resizing/copy-6.svg)

This algorithm produces a valid robin hood hashtable which contains all the entries since the entire procedure is actually just 
inserting entries into the new hashtable using the traditional robin hood insert method 
but in a particular order that makes it so we don't have to do any swapping. 
And that's the fast resizing algorithm! 
